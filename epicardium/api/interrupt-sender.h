#pragma once
#include "api/common.h"

void api_interrupt_init(void);
bool api_interrupt_is_ready(void);
void api_interrupt_trigger(api_int_id_t id);
