``config`` - Configuration
==========================
The ``config`` module provides functions to interact with card10's
configuration file (``card10.cfg``).

.. automodule:: config
   :members:


