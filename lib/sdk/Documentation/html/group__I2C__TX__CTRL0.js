var group__I2C__TX__CTRL0 =
[
    [ "MXC_F_I2C_TX_CTRL0_TX_PRELOAD_POS", "group__I2C__TX__CTRL0.html#ga74630a954110bef5ec0a34c8e450c601", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_PRELOAD", "group__I2C__TX__CTRL0.html#ga90b0482fe14bbeae88f69d1fa8e8cf16", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_READY_MODE_POS", "group__I2C__TX__CTRL0.html#ga3042941ff5a0bc134e586ce2fde5d9a9", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_READY_MODE", "group__I2C__TX__CTRL0.html#ga26af00f81b0cd84aba67440058a022b8", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMGC_AFD_POS", "group__I2C__TX__CTRL0.html#ga0923689f25a83e897638b658f0a16fd9", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMGC_AFD", "group__I2C__TX__CTRL0.html#ga189de16f7f78a1d65e9aece0bd5d4116", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMW_AFD_POS", "group__I2C__TX__CTRL0.html#ga3e9fae8b136784898c9d6daf2f331cf4", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMW_AFD", "group__I2C__TX__CTRL0.html#ga4543f5eb5c086a1133d624b6162981f0", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMR_AFD_POS", "group__I2C__TX__CTRL0.html#gaceb8864ca1bda9d0dec92142cae7211e", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_AMR_AFD", "group__I2C__TX__CTRL0.html#ga9746703e5e01e63d5496f33b1b200b92", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_NACK_AFD_POS", "group__I2C__TX__CTRL0.html#gaa570eab7523c71331acccd2c130bcea1", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_NACK_AFD", "group__I2C__TX__CTRL0.html#ga61fd78f35e387d504135528f4d08359e", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_FLUSH_POS", "group__I2C__TX__CTRL0.html#gad48be875120e447870026e793f4218ac", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_FLUSH", "group__I2C__TX__CTRL0.html#gaadd0eafb703ccd371b9dc8c03194e84a", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_THRESH_POS", "group__I2C__TX__CTRL0.html#gaad2d44565b2018ccb3f69d3d2fc3aa0b", null ],
    [ "MXC_F_I2C_TX_CTRL0_TX_THRESH", "group__I2C__TX__CTRL0.html#ga7273cbba4414015d12e69105890a73de", null ]
];