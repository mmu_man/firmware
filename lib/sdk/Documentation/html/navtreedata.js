var NAVTREE =
[
  [ "MAX32665 SDK Documentation", "index.html", [
    [ "License Agreement", "index.html#license", null ],
    [ "API Overview and Usage", "md_API_Overview_and_Usage.html", [
      [ "Introduction", "md_API_Overview_and_Usage.html#introPage", null ],
      [ "CMSIS", "md_API_Overview_and_Usage.html#cmsisPage", [
        [ "Register-level Header Files", "md_API_Overview_and_Usage.html#regHeadersPage", null ],
        [ "Startup Code", "md_API_Overview_and_Usage.html#startupCodePage", null ],
        [ "Peripheral Drivers", "md_API_Overview_and_Usage.html#periphDriverPage", null ],
        [ "System Layer", "md_API_Overview_and_Usage.html#sysLayerPage", null ],
        [ "Board Support Package", "md_API_Overview_and_Usage.html#bspPage", null ]
      ] ],
      [ "Synchronous vs. Asynchronous APIs", "md_API_Overview_and_Usage.html#asyncAPIPage", null ],
      [ "Low-power API Considerations", "md_API_Overview_and_Usage.html#lpPage", null ],
      [ "Debug Assertions", "md_API_Overview_and_Usage.html#debugAssertPage", null ],
      [ "Makefile Structure and Building Projects", "md_API_Overview_and_Usage.html#makefilePage", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__DMA__CFG.html#gaa40064aedd82461156cafea9f28b5637",
"group__GPIO__EN1.html",
"group__I2C__CTRL.html#ga39579e0316982d769be0e9b3299f5fd0",
"group__MX25__Commands.html#gaa2b793695f0aa11fe153dc7061076078",
"group__PWRSEQ__LPMEMSD.html#gaa8765e2510eff852c4aed995dcf6485b",
"group__RPU__FCR.html#ga5de525c2839090dd2022ee57d6ef063f",
"group__RPU__MCR.html#ga907430c6844334d7e873eb4280c70fd9",
"group__RPU__SFCC.html#ga680548f12d0eb1f7a221e14b09f99474",
"group__RPU__SRCC.html#gad3022f0939c58a7a5f7c08d902e538de",
"group__RPU__SYSRAM6.html#ga91432447e5d304186ad50e121dc50dd9",
"group__RPU__WDT0.html#ga36ed881c254b006455e6cad04e2377ca",
"group__SDHC__CMD.html#gaff22158cf963f95fb2ba2966810240ab",
"group__SDHC__PRESET__0.html#gad9c8ae1d5bc075d7e29126a857588965",
"group__SPI17Y__DMA.html#ga33b184f6d143129c30e0beaf1fbac297",
"group__SPIXFC__INTFL.html#ga375daa03fbff1a3ff858a2b70343f453",
"group__SPIXR__CTRL3.html#gad3dbfc4e70c697e2bb87830fbdc1d7bf",
"group__TPU__CIPHER__CTRL.html#gae2a24a0893d0d755e2a7a3d6ca7ee25c",
"group__UART__CTRL.html#ga1c09238a1318f0eb0649dfb400adf566",
"group__cipher.html#ga72161088b0de9ed22c77f09c31612963",
"group__i2c.html#ga9140635155288386fd92181b540ac42c",
"group__sdhc.html#ga24e83f1ade99d0030cedc3bc31e26c68",
"group__wdt.html#ggaedf61fda1bc2197dc63124d697b89a4cacdae184a32e60cc2d07fef5ca8d72d3c",
"structmxc__rpu__regs__t.html#a938e8c511fe710606bb950a76477b9a2",
"structspixf__req__t.html#a2a03a5ac2c42c36b1f9860b9010d7284"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';