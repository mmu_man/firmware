import os
import display
import time
import buttons
import sys_ble
import interrupt

CONFIG_NAME = "ble.txt"
MAC_NAME = "mac.txt"
ACTIVE_STRING = "active=true"
INACTIVE_STRING = "active=false"
ble_event = None


def ble_callback(_):
    global ble_event
    ble_event = sys_ble.get_event()


def init():
    if CONFIG_NAME not in os.listdir("."):
        with open(CONFIG_NAME, "w") as f:
            f.write(INACTIVE_STRING)
    interrupt.set_callback(interrupt.BLE, ble_callback)
    interrupt.enable_callback(interrupt.BLE)
    sys_ble.set_bondable(True)


def load_mac():
    if MAC_NAME in os.listdir("."):
        with open(MAC_NAME) as f:
            return f.read().strip()


def triangle(disp, x, y, left):
    yf = 1 if left else -1
    scale = 6
    disp.line(x - scale * yf, int(y + scale / 2), x, y)
    disp.line(x, y, x, y + scale)
    disp.line(x, y + scale, x - scale * yf, y + int(scale / 2))


def toggle():
    content = INACTIVE_STRING if is_active() else ACTIVE_STRING
    with open(CONFIG_NAME, "w") as f:
        f.write(content)

    disp.clear()
    disp.print("resetting", posy=0, fg=[0, 255, 255])
    disp.print("to toggle", posy=20, fg=[0, 255, 255])
    disp.print("BLE state", posy=40, fg=[0, 255, 255])
    disp.update()
    os.reset()


def is_active():
    with open(CONFIG_NAME, "r") as f:
        state = f.readlines()[0]
        if len(state) < len(ACTIVE_STRING):
            return False
        state = state[0 : len(ACTIVE_STRING)]
        return state == ACTIVE_STRING


def headline():
    disp.print("BLE", posy=0, fg=[0, 255, 255])
    if is_active():
        disp.print("active", posy=20, fg=[0, 255, 0])
        mac = load_mac()
        if mac is not None:
            disp.print(mac[9:], posy=60, fg=[0, 0, 255])
    else:
        disp.print("inactive", posy=20, fg=[255, 0, 0])


def selector():
    triangle(disp, 148, 46, False)
    disp.print("toggle", posx=25, posy=40, fg=[255, 255, 255])


init()
disp = display.open()
state = 1
v_old = buttons.read()

while True:
    v_new = buttons.read()
    v = ~v_old & v_new
    v_old = v_new

    if state == 1:
        # print config screen
        disp.clear()
        headline()
        selector()
        disp.update()
        state = 2
    elif state == 2:
        # wait for button press or ble_event
        if ble_event == sys_ble.EVENT_HANDLE_NUMERIC_COMPARISON:
            ble_event = None
            state = 3
        if v & buttons.TOP_RIGHT:
            toggle()
            state = 1

    elif state == 3:
        # print confirmation value
        compare_value = sys_ble.get_compare_value()
        disp.clear()
        disp.print("BLE: Bond?", posy=0, fg=[0, 0, 255])
        disp.print("Code:", posy=20, fg=[0, 255, 255])
        disp.print("   %06d" % compare_value, posy=40, fg=[255, 255, 255])
        disp.print("Yes", posy=60, fg=[0, 255, 0])
        disp.print("No", posx=120, posy=60, fg=[255, 0, 0])

        disp.update()
        state = 4
    elif state == 4:
        # wait for button press or ble_event
        if ble_event == sys_ble.EVENT_PAIRING_FAILED:
            ble_event = None
            state = 6
        if v & buttons.BOTTOM_LEFT:
            sys_ble.confirm_compare_value(True)
            disp.clear()
            disp.print("BLE Bonding", posy=0, fg=[0, 0, 255])
            disp.print("Please Wait", posy=40, fg=[255, 255, 255])
            disp.update()
            state = 5
        elif v & (buttons.BOTTOM_RIGHT | buttons.TOP_RIGHT):
            sys_ble.confirm_compare_value(False)
            state = 6

    elif state == 5:
        # Wait for pairing to complete
        if ble_event == sys_ble.EVENT_PAIRING_FAILED:
            ble_event = None
            state = 6
        elif ble_event == sys_ble.EVENT_PAIRING_COMPLETE:
            ble_event = None
            disp.clear()
            disp.print("BLE Bonding", posy=0, fg=[0, 0, 255])
            disp.print("  Success", posy=40, fg=[0, 255, 0])
            disp.update()
            time.sleep(5)
            os.exec("main.py")

    elif state == 6:
        # display fail screen and wait 5 seconds
        disp.clear()
        disp.print("BLE Bonding", posy=0, fg=[0, 0, 255])
        disp.print("   Fail", posy=40, fg=[255, 0, 0])
        disp.update()
        time.sleep(5)
        state = 1

    time.sleep(0.1)
